<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Course;
use Illuminate\Http\Request;


Route::get('/', ['as' => 'courses', 'uses' => 'CourseController@index']);

Route::get('view/{id}', ['as' => 'course', 'uses' => 'CourseController@view']);


Route::get('course/create', ['as' => 'course.create', 'uses' => 'CourseController@create']);

Route::post('course/store', ['as' => 'course.store', 'uses' => 'CourseController@store']);

Route::delete('view/{id}', ['as' => 'course.delete', 'uses' => 'CourseController@delete']);


Route::get('{id}/section/create', ['as' => 'section.create', 'uses' => 'CourseController@sectionCreate']);

Route::post('section/store', ['as' => 'section.store', 'uses' => 'CourseController@sectionStore']);

Route::delete('section/{id}', ['as' => 'section.delete', 'uses' => 'CourseController@sectionDelete']);


Route::get('{courses_id}/{sections_id}/lesson/create', ['as' => 'lesson.create', 'uses' => 'CourseController@lessonCreate']);

Route::post('lesson/store', ['as' => 'lesson.store', 'uses' => 'CourseController@lessonStore']);

Route::delete('lesson/{id}', ['as' => 'lesson.delete', 'uses' => 'CourseController@lessonDelete']);