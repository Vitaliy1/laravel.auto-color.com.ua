<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\Section;
use App\Models\Lesson;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call('CoursesSeeder');
        $this->call('SectionsSeeder');
        $this->call('LessonsSeeder');
    }
}

class CoursesSeeder extends Seeder {

    public function run()
    {
        DB::table('Courses')->delete();
        Course::create([
            'name' => 'Microsoft Excel для бизнес-аналитика',
            'image' => 'excel.jpeg'
        ]);

        Course::create([
            'name' => 'Yii для начинающих',
            'image' => 'laravel.png'
        ]);

        Course::create([
            'name' => 'Laravel для начинающих',
            'image' => 'yii.png'
        ]);
    }
}

class SectionsSeeder extends Seeder {

    public function run()
    {
        DB::table('Sections')->delete();
        Section::create([
            'name' => 'О курсе',
            'description' => 'Вступление курса Microsoft Excel и разъяснения, кому он полезен',
            'courses_id' => 1
        ]);    

        Section::create([
            'name' => 'Сценании использования',
            'description' => 'Основные сценании использования Эксель в бизнес-анализе',
            'courses_id' => 1
        ]);    

        Section::create([
            'name' => 'Спец. возможности',
            'description' => 'Специальные возможности для продвинутых',
            'courses_id' => 1
        ]);        

//
        Section::create([
            'name' => 'Введение',
            'description' => 'О чем нибудь вступительном...',
            'courses_id' => 2
        ]);    

        Section::create([
            'name' => 'Первое знакомство',
            'description' => 'Установка, формы и другое...',
            'courses_id' => 2
        ]);  
    }
}

class LessonsSeeder extends Seeder {

    public function run()
    {
        DB::table('Lessons')->delete();
        Lesson::create([
            'name' => 'Что такое Эксель',
            'price' => 20,
            'sections_id' => 1
        ]);    

        Lesson::create([
            'name' => 'Почему это идеальный инструмент бизнес-аналитика',
            'price' => 20,
            'sections_id' => 1
        ]);  

        Lesson::create([
            'name' => 'Отчеты по процессам',
            'price' => 30,
            'sections_id' => 2
        ]);   

        Lesson::create([
            'name' => 'Data Mining',
            'price' => 30,
            'sections_id' => 2
        ]);   

        Lesson::create([
            'name' => 'Оптимизация',
            'price' => 30,
            'sections_id' => 2
        ]);  

        Lesson::create([
            'name' => 'Формулы для массивов',
            'price' => 50,
            'sections_id' => 3
        ]);   

        Lesson::create([
            'name' => 'Макросы и VBA',
            'price' => 50,
            'sections_id' => 3
        ]);           

        //
        Lesson::create([
            'name' => 'О Yii',
            'price' => 15,
            'sections_id' => 4
        ]);  

        Lesson::create([
            'name' => 'Обновление версии',
            'price' => 24,
            'sections_id' => 4
        ]);   

        Lesson::create([
            'name' => 'Установка Yii',
            'price' => 36,
            'sections_id' => 5
        ]);           
    }
}