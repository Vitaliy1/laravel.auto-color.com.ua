@extends('app')

@section('content')

	<div class="row">
       <div class="col-xs-12 text-right">       		
	       	<h2 style="text-decoration: underline;">{!! link_to_route('course.create', 'Создать новый курс?') !!}</h2>			
       </div>
	</div>
	<br />
	@foreach($courses as $course)
		<article>
			<h2><a href="{!! action('CourseController@view', ['id' => $course->id]); !!}">{{ $course->name }}</a></h2>
			@if($course->image)
				<p>
					<a href="{!! action('CourseController@view', ['id' => $course->id]); !!}"><img src="<?= url('/images') ?>/{{ $course->image }}" style="max-width: 300px; max-height: 300px;"></a>
				</p>
			@endif
			<p>
				Общая стоимость на курс составляет	{{ $course->price }} грн.
			</p>
			<p class="form-group">
				<form action="{{ url('/view/'.$course->id) }}" method="POST">
		            {!! csrf_field() !!}
		            {!! method_field('DELETE') !!}

		            <button type="submit" class="btn btn-danger">
		                <i class="fa fa-trash"></i> Удалить курс
		            </button>
		        </form>
			<p>
		</article>
		<br />
	@endforeach
@stop