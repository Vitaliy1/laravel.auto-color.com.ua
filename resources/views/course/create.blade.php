@extends('app')

@section('content')

    @php
        $breadcrumbs->setDivider('»');
        echo $breadcrumbs->render();
    @endphp

    <h2>Создать новый курс</h2>

    @if (count($errors) > 0)
    	<div class="alert alert-danger">
			<ul>
  				@foreach ($errors->all() as $error)
    				<li>{{ $error }}</li>
  				@endforeach
			</ul>
		</div>
    @endif
    
    {!! Form::open(['route' => 'course.store', 'files' => true]) !!}

        @include('course._form')
    {!! Form::close()!!}
@endsection