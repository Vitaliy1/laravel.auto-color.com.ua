@extends('app')

@section('content')

	@php
		$breadcrumbs->setDivider('»');
		echo $breadcrumbs->render();
    @endphp

	<div class="row">
		<div class="col-xs-9">
			<h2>{{ $course->name }}</h2>
		</div>
       <div class="col-xs-3">       		
	       	<a class="btn btn-success" href="{{ url($course->id.'/section/create') }}">Добавить секцию</a>	
       </div>
	</div>
	
	@if($course->image)
		<p>
			<img src="<?= url('/images') ?>/{{ $course->image }}" style="max-width: 300px; max-height: 300px;">
		</p>
	@endif
	@foreach($sections as $section)
		<article>
			<div class="row">				
				<div class="col-xs-8">
					<h3>{{ $section->name }} ({{ $section->price }})</h3>
				</div>

				<div class="col-xs-4">
					<p class="form-group">
						<form action="{{ url('/section/'.$section->id) }}" method="POST">
				            {!! csrf_field() !!}
				            {!! method_field('DELETE') !!}
				            <button type="submit" class="btn btn-danger">
				                <i class="fa fa-trash"></i> Удалить секцию
				            </button>
				        </form>
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-7">
					<p><i>{{ $section->description }}</i></p>
				</div>

				<div class="col-xs-5">       		
			       	<a class="btn btn-success" href="{{ url($course->id.'/'.$section->id.'/lesson/create') }}" style="margin-bottom:10px">Добавить урок</a>	
		       </div>
		    </div>
			
			@foreach($section->lessons as $lesson)
				<div class="row">
					<div class="col-xs-7">
						<ul>
							<li>{{ $lesson->name }} ({{ $lesson->price }})</li>
						</ul>
					</div>
					<div class="col-xs-5">
						<p class="form-group" style="margin:0">
							<form action="{{ url('/lesson/'.$lesson->id) }}" method="POST" style="margin:0">
					            {!! csrf_field() !!}
					            {!! method_field('DELETE') !!}
					            <button type="submit" class="btn btn-danger">
					                <i class="fa fa-trash"></i> Удалить урок
					            </button>
					        </form>
						</p>
					</div>
				</div>
			@endforeach
			
		</article>
	@endforeach
@stop