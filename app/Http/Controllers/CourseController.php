<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Section;
use App\Models\Lesson;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CourseController extends Controller
{
	public function __construct() {
	    $this->breadcrumbs = new \Creitive\Breadcrumbs\Breadcrumbs;
	    $this->breadcrumbs->addCrumb('Курсы', url('/'));

	    //$this->$breadcrumbs->setListElement('ol');
	}

    public function index() {
    	$courses = Course::all();

		foreach ($courses as $course) {
			$course->price = DB::table('sections')
            	->join('lessons', 'sections.id', '=', 'lessons.sections_id')
            	->where('sections.courses_id', '=', $course->id)
            	->sum('price');
		}

    	return view('course.index', ['courses' => $courses]);
    }

    public function view($id) {
    	$course = Course::find($id);

    	$sections = Section::where('courses_id', '=', $id)->get();

    	foreach ($sections as $section) {
    		$lessons = Lesson::where('sections_id', '=', $section->id);
    		$section->lessons = $lessons->get();
    		$section->price = $lessons->sum('price');
    	}

    	$this->breadcrumbs->addCrumb('Курс '.$course->name, url('/').'/view/'.$id);

    	return view('course.view', ['course' => $course, 'sections' => $sections, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function create() {
    	$this->breadcrumbs->addCrumb('Создать курс', url('/').'/course/create');
	    return view('course.create', ['breadcrumbs' => $this->breadcrumbs]);
	}

	public function store(Request $request) {
	    $this->validate($request, [
			'name' => 'required|max:255',
			'image' => 'image'
		]);

	    $course = new Course;
	    $course->name = $request->name;

	    if (Input::hasFile('image')) {

	    	$destinationPath = 'images';
	    
	    	$fileExtension = Input::file('image')->getClientOriginalExtension();
	    	$fileName = rand(11111,99999).'.'.$fileExtension;

	    	Input::file('image')->move($destinationPath, $fileName);

	    	$course->image = $fileName;
	    }
	    
	    $course->save();
	    
	    return redirect('/');
	}

	public function delete($id) {
		$course = Course::find($id);

		if ($course->image) {
			unlink(public_path().'/images/'.$course->image);
		}
		
		$course->delete();

	    return redirect('/');
	}

	public function sectionCreate($courses_id) {
		$course = Course::find($courses_id);
		$this->breadcrumbs
			->addCrumb('Курс '.$course->name, url('/').'/view/'.$courses_id)
			->addCrumb('Создать секцию', url('/').'/'.$courses_id.'/section/create');
	    return view('section.create', ['courses_id' => $courses_id, 'breadcrumbs' => $this->breadcrumbs]);
	}

	public function sectionStore(Request $request) {
		$this->validate($request, [
			'name' => 'required|max:255'
		]);

	    $section = new Section;
	    $section->name = $request->name;
	    $section->description = $request->description;	    
	    $section->courses_id = $request->courses_id;	
	    
	    $section->save();
	    
	    return redirect('view/'.$section->courses_id);
	}

	public function sectionDelete($id) {
		$section = Section::find($id);

		$section->delete();

	    return redirect('view/'.$section->courses_id);
	}

	public function lessonCreate($courses_id, $sections_id) {
		$course = Course::find($courses_id);
		$section = Section::find($sections_id);
		$this->breadcrumbs
			->addCrumb('Курс '.$course->name.' (Секция '.$section->name.')', url('/').'/view/'.$courses_id)		
			->addCrumb('Создать урок', url('/').'/'.$courses_id.'/'.$sections_id.'/lesson/create');
	    return view('lesson.create', ['courses_id' => $courses_id, 'sections_id' => $sections_id, 'breadcrumbs' => $this->breadcrumbs]);
	}

	public function lessonStore(Request $request) {
		$this->validate($request, [
			'name' => 'required|max:255',
			'price' => 'required|integer|min:0'
		]);

	    $lesson = new Lesson;
	    $lesson->name = $request->name;
	    $lesson->price = $request->price;	    
	    $lesson->sections_id = $request->sections_id;	
	    $courses_id = $request->courses_id;	
	    
	    $lesson->save();
	    
	    return redirect('view/'.$courses_id);
	}

	public function lessonDelete($id) {
		$lesson = Lesson::find($id);
		
        $section = Section::find($lesson->sections_id);
		
		$lesson->delete();

	    return redirect('view/'.$section->courses_id);
	}
}
